const Bundler = require('parcel-bundler');
const express = require('express');
const {createProxyMiddleware} = require('http-proxy-middleware');

const app = express();

app.use('/autoapi/', createProxyMiddleware({
  target: 'http://127.0.0.1:8000/',
}));

const bundler = new Bundler('spa.html');
app.use(bundler.middleware());

app.listen(Number(process.env.PORT || 1234));
