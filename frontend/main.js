import React from 'react';
import ReactDOM from 'react-dom';

const el = document.getElementById('main');

const URL = '/autoapi/expense/';

const Main = () => {
  const [expenses, setExpenses] = React.useState();

  React.useEffect(() => {
    fetch(URL).then(r=>r.json()).then(setExpenses);
  }, []);

  return <div>
    {expenses ? <ul>
      {expenses.map(x => <li key={x.id}>
        {x.title} {x.amount}
      </li>)}
    </ul> : 'LOADING....'}
  </div>;
};

ReactDOM.render(<Main></Main>, el);
