from django.test import TestCase

from pokemons.models import Pokemon


class PokemanTest(TestCase):
    def test_models(self):
        data = [
            ("Pikachu", 6),
            ("Kadabra", 2),
            ("Koffing", 12),
        ]
        for name, level in data:
            o, created = Pokemon.objects.get_or_create(
                name=name,
                defaults={
                    'level': level,
                }
            )

        self.assertEqual(Pokemon.objects.count(), len(data))
