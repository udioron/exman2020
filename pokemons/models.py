from django.db import models


class Pokemon(models.Model):
    name = models.CharField(max_length=100, unique=True)
    level = models.IntegerField(default=1)

