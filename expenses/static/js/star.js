'use strict';

console.log('STAR START');

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
const csrftoken = getCookie('csrftoken');

const stars = document.querySelectorAll('.star');

stars.forEach(star => {

  let sent = false;

  const url = star.dataset.url;
  // console.log(url);

  star.addEventListener('click', () => {
    console.log('CLICK START');
    if (sent) {
      return;
    }

    star.innerText = '♻';
    sent = true;
    const config = {
      method: 'post',
      headers: {
        'X-CSRFToken': csrftoken,
      }
    };
    const my_promise = fetch(url, config);
    my_promise.then(resp => {
          if (resp.status !== 200) {
            alert(`Server error: Got ${resp.status}. Please try again later`);
            return;
          }
          return resp.json();
        },
    ).then(data => {
      console.log('SUCCESS START');
      console.log(data);
      star.innerText = '🌟';
      sent = false;
      console.log('SUCCESS END');
    });
    my_promise.catch(
        () => alert('Cannot connect to server. Please try again later.'));

    console.log('CLICK END');
  });
});

console.log('STAR END');
