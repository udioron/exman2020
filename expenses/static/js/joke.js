'use strict';

console.log('JOKE START');

const btn = document.getElementById('get-joke');
const el = document.getElementById('joke');
// const url = 'https://icanhazdadjoke.com/';
// const url = 'https://www.ynet.co.il/asdsakjdh.json';
const url = '/expenses/best-joke-api/';

btn.addEventListener('click', () => {
  console.log('CLICK START');

  el.innerText = 'Loading....';
  btn.setAttribute('disabled', 1);

  const config = {
    headers: {
      Accept: 'application/json',
    },
  };

  fetch(url, config).then(resp => resp.json()).then(data => {
    console.log('SUCCESS START');
    console.log(data);
    el.innerText = data.joke;
    btn.removeAttribute('disabled');
    console.log('SUCCESS END');
  });

  console.log('CLICK END');
});

console.log('JOKE END');
