'use strict';

console.log('TRIVIA START');

const url = 'https://opentdb.com/api.php?amount=1';

const btn = document.getElementById('new-question');
const el = document.getElementById('question');

btn.addEventListener('click', () => {
  fetch(url).then(resp => resp.json()).then(data => {
    console.log(data);
    el.innerHTML = unescape(data.results[0].question);
  });
});

console.log('TRIVIA END');
