const el = document.getElementById('main');

const CATEGORY_URL = '/autoapi/category/';

const getCategories = () => fetch(CATEGORY_URL).then(r => r.json());

const Main = () => {
  const [amount, setAmount] = React.useState(17);
  const [categories, setCategories] = React.useState([]);

  React.useEffect(() => {
    getCategories().then(resp => {
      console.log(resp);
      setCategories(resp);
    });
  }, []);

  const setRandomAmount = (e) => {
    setAmount(Math.random() * 100);
    e.preventDefault();
  };

  const submit = (e) => {
    console.log('SUBMIT');
    const data = {amount, title:"asdksajhd", date:"2020-1-1", category:12};
    const config = {
      method: 'POST',
      headers: {
        'X-CSRFToken': csrftoken,
      }
      body: new FormData(data),
      // body: new JSON.stringify(data),
    };
    setSending(true);
    fetch('', config).then(() => ...);
    e.preventDefault();
  };

  return <div>
    <form method="post" onSubmit={submit}>
      <div className="form-group"><label
          htmlFor="id_category">Category</label>
        {categories.length ?
            <select name="category"
                    className="form-control"
                    title="" required
                    id="id_category">

              {categories.map(
                  x => <option key={x.id} value={x.id}>{x.name}</option>)}
            </select> : 'loading categories'}
      </div>
      <div className="form-group"><label htmlFor="id_date">Date</label><input
          type="text" name="date" className="form-control" placeholder="Date"
          title="" required id="id_date"/></div>
      <div className="form-group"><label
          htmlFor="id_amount">Amount</label><input type="number" name="amount"
                                                   step="0.01"
                                                   className="form-control"
                                                   placeholder="Amount"
                                                   title="" required
                                                   id="id_amount"
                                                   value={amount}
                                                   onChange={e => setAmount(
                                                       e.target.value)}

      /></div>
      <div>
        <button onClick={setRandomAmount}>Random amount</button>
      </div>
      <div className="form-group"><label
          htmlFor="id_title">Title</label><input type="text" name="title"
                                                 maxLength="200"
                                                 className="form-control"
                                                 placeholder="Title" title=""
                                                 required id="id_title"/></div>
      <div className="form-group"><label
          htmlFor="id_description">Description</label><textarea
          name="description" cols="40" rows="10" className="form-control"
          placeholder="Description" title="" required id="id_description">
</textarea></div>
      <div className="form-group">
        <div className="form-check"><input type="checkbox" name="is_star"
                                           className="form-check-input"
                                           id="id_is_star"/><label
            className="form-check-label" htmlFor="id_is_star">Is star</label>
        </div>
      </div>
      <button className="btn btn-primary">Add</button>
    </form>
  </div>;
  ;
};

console.log(el);
ReactDOM.render(<Main/>, el);
