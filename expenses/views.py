import logging
import logging
import random

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse, HttpResponseNotAllowed
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import ListView
from rest_framework.viewsets import ModelViewSet

from expenses import forms
from expenses.models import Expense, Category
from expenses.serializers import ExpenseSerializer, CategorySerializer

logger = logging.getLogger(__name__)


def joke_view(request):
    return render(request, "jokes.html", {

    })


def best_joke_json_view(request):
    # import time
    # time.sleep(3)
    n = random.randint(2, 100)
    payload = {
        'id': "sakdjhaskjdh",
        'joke': f'How can you put {n} elephants in a beetle?',
    }
    return JsonResponse(payload)


# @login_required
# def expense_list(request):
#     qs = Expense.objects.filter(user=request.user).order_by('-date')[:12]
#     # FIXME: use sql group by sum???
#     total = sum([x.amount for x in qs])
#     return render(request, "expenses/expense_list.html", {
#         'object_list': qs,
#         'total': total,
#     })

#
# see: https://docs.djangoproject.com/en/3.0/topics/class-based-views/ and https://ccbv.co.uk/
class ExpenseListView(LoginRequiredMixin, ListView):
    def get_queryset(self):
        return Expense.objects.filter(user=self.request.user).order_by('-date')

    def total(self):
        qs = self.get_queryset()
        return sum([x.amount for x in qs])


@login_required
def expense_list(request):
    qs = Expense.objects.filter(user=request.user).order_by('-date')[:12]
    # FIXME: use sql group by sum???
    total = sum([x.amount for x in qs])
    return render(request, "expenses/expense_list.html", {
        'object_list': qs,
        'total': total,
    })


@login_required
def expense_detail(request, id):
    o = get_object_or_404(Expense, id=id, user=request.user)

    return render(request, "expenses/expense_detail.html", {
        'object': o,
    })


@login_required
def expense_star(request, id):
    if request.method != "POST":
        return HttpResponseNotAllowed(['POST'])
    import time
    time.sleep(2.5)
    o = get_object_or_404(Expense, id=id)
    logger.warning(f"STARING EXPENSE #{id}, {o}")
    o.is_star = True
    o.save()
    return JsonResponse({'status': 'OK'})


@login_required
def expense_create(request):
    # if not request.user.userprofile.is_manager:
    #     return redirect("hell")
    if request.method == "POST":
        form = forms.ExpenseForm(request.POST)
        if form.is_valid():
            # data = form.cleaned_data
            form.instance.user = request.user
            o = form.save()
            messages.success(request, f"Expense #{o.id} added. Thank you so very much!!!!!")
            # return redirect(o)  # TODO: implement get_absolute_url
            return redirect("expenses:list")

    else:
        form = forms.ExpenseForm()
    return render(request, "expenses/expense_form.html", {
        'form': form,
    })


@login_required
def expense_create_react(request):
    # if not request.user.userprofile.is_manager:
    #     return redirect("hell")
    if request.method == "POST":
        form = forms.ExpenseForm(request.POST)
        if form.is_valid():
            # data = form.cleaned_data
            form.instance.user = request.user
            o = form.save()
            messages.success(request, f"Expense #{o.id} added. Thank you so very much!!!!!")
            # return redirect(o)  # TODO: implement get_absolute_url
            return redirect("expenses:list")

    else:
        form = forms.ExpenseForm()
    return render(request, "expenses/expense_form_react.html", {
        'form': form,
    })


@login_required
def expense_json_list(request):
    qs = Expense.objects.filter(user=request.user).order_by('-date')[:12]
    ser = ExpenseSerializer(qs, many=True)
    return JsonResponse({'expenses': ser.data})


class ExpenseViewSet(ModelViewSet):
    serializer_class = ExpenseSerializer
    queryset = Expense.objects.all()

class CategoryViewSet(ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
