import random

import silly
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from expenses.models import Expense, Category


def get_description():
    return "\n".join([silly.sentence() for i in range(random.randint(1, 3))])


class Command(BaseCommand):
    help = "Create some silly expensese"

    def add_arguments(self, parser):
        parser.add_argument('n', type=int)

    def handle(self, n, *args, **options):
        if User.objects.count() < 10:
            for i in range(10):
                un = f"user{i}"
                User.objects.create_user(
                    username=un,
                    password=un,
                )
        # Expense.objects.all().delete()
        for i in range(6):
            Category.objects.create(name=silly.name(), priority=random.randint(1, 10))

        cats = Category.objects.all()

        for i in range(n):
            u = User.objects.order_by("?").first()
            o = Expense(
                user=u,
                category=Category.objects.order_by("?").first(),
                # category=random.choice(cats),
                amount=random.uniform(7, 950),
                title=silly.thing(),
                date=silly.datetime(),
                description=get_description(),
            )
            o.save()
