from django.contrib.auth.models import User
from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=200)
    priority = models.IntegerField(default=5)

    def __str__(self):
        return self.name

    def is_important(self):
        return self.priority <= 2


class Expense(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT, related_name="expenses")
    category = models.ForeignKey(Category, on_delete=models.PROTECT,
                                 related_name="expenses")
    date = models.DateField()
    amount = models.DecimalField(decimal_places=2, max_digits=12)
    title = models.CharField(max_length=200)
    description = models.TextField()
    is_star = models.BooleanField(default=False)

    def __str__(self):
        return f"[#{self.id}] ${self.amount}@{self.date}: {self.title}"
