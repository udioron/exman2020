from rest_framework.serializers import ModelSerializer

from . import models


class ExpenseSerializer(ModelSerializer):
    class Meta:
        model = models.Expense
        fields = '__all__'

class CategorySerializer(ModelSerializer):
    class Meta:
        model = models.Category
        fields = '__all__'

