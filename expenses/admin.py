from django.contrib import admin

from expenses import models


class ExpenseAdmin(admin.ModelAdmin):
    date_hierarchy = 'date'
    search_fields = (
        'id',
        'title',
        'amount',
        'description',
    )
    list_display = (
        'id',
        'title',
        'amount',
        'date',
        'category',
    )
    list_filter = (
        'category',
    )


admin.site.register(models.Expense, ExpenseAdmin)
admin.site.register(models.Category)
