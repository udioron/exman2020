from django.urls import path, include
from rest_framework import routers

from . import views

app_name = "expenses"

router = routers.DefaultRouter()
router.register(r'expense', views.ExpenseViewSet)
router.register(r'category', views.CategoryViewSet)

urlpatterns = [
    # path('', views.expense_list, name="list"),
    path('', views.ExpenseListView.as_view(), name="list"),
    path('joke/', views.joke_view, name="joke"),
    path('best-joke-api/', views.best_joke_json_view, name="joke_json"),
    # path('trivia/', views.trivia_view, name="trivia/"),
    path('create/', views.expense_create, name="create"),
    path('create-react/', views.expense_create_react, name="create_react"),
    path('<int:id>/', views.expense_detail, name="detail"),
    path('<int:id>/star/', views.expense_star, name="star"),

    path('api/expenses/', views.expense_json_list, name="json_list"),
    path('autoapi/', include(router.urls)),
]
