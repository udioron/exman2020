from django.urls import path

from . import views

app_name = "feedback"

urlpatterns = [
    path('contact-us/', views.contact_us_view, name="contact"),
]
