from django import forms


class ContactUsForm(forms.Form):
    birthday = forms.DateField()
    name = forms.CharField(max_length=100)
    email = forms.EmailField(max_length=100)
    number_of_children = forms.IntegerField(initial=3)
    message = forms.CharField(widget=forms.Textarea())
