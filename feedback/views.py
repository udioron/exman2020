from django.contrib import messages
from django.core.mail import mail_admins
from django.shortcuts import render, redirect

from . import forms


def contact_us_view(request):
    if request.method == "POST":
        form = forms.ContactUsForm(request.POST)
        ## form is a bound form
        if form.is_valid():
            # print(request.POST)
            # print(form.cleaned_data)

            # 3: POST + VALID:
            data = form.cleaned_data
            # o = Feedback()
            # o.birthday = data['birthday']
            # o.save()
            mail_admins(f"feedback from {data['name']}", data['message'])
            messages.success(request, "Your feeback has been sent")
            return redirect("expenses:list")

        # 2: POST+invalid ... next line: render.
    else:
        # 1 GET
        form = forms.ContactUsForm()
        # form is unbound
    return render(request, "feedback/contact_us_form.html", {
        'form': form,
    })
